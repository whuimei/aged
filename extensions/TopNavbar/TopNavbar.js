/*globals $:false */
/*globals elasticlunr:false */
"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function do_search(index, searchCtrl) {
  var searchResult = index.search(searchCtrl, {
    fields: {
      title: {
        boost: 2,
        bool: "AND"
      },
      body: {
        boost: 1
      }
    },
    boolean: "OR",
    expand: true
  }) || [];
  $('#search-results').empty();
  /* celar old results */

  $('#search-results').removeClass('is-active');

  for (var i = 0; i < searchResult.length; i++) {
    var result = search_index.documentStore.docs[searchResult[i].ref];
    var qt = result.title;
    var ql = result.url;
    var q = result.description;
    $('#search-results').append('<div><a href="' + ql + '">' + qt + '</a></div>' + '<div><small>' + q + '</small></div><hr>');
    /* open the suggestion list */

    $('#search-results').addClass('is-active');
  }
  return searchResult;
}

window.onresize = function (event) {
  if (window.outerWidth > 768) {
    $("#main-nav > ul").slideDown();
  } else {
    $("#main-nav > ul").slideUp();
  }
};

function toggle_main_nav() {
  $("#main-nav > ul").slideToggle();
}

function highlightFirstActive() {
  var visibleLink = null;
  var sectlevel = null;

  $(".is-visible").each(function (i, link) {
    if(sectlevel == null || link.parentNode.parentNode.className > sectlevel) {
      sectlevel = link.parentNode.parentNode.className;
      visibleLink = link;
    }
  });

  if (visibleLink) {
    visibleLink.classList.add('active');

    while (visibleLink != null && !visibleLink.classList.contains("sectlevel1")) {
      if (visibleLink.parentNode.tagName === 'LI') {
        visibleLink.parentNode.classList.add('open');
      }

      visibleLink = visibleLink.parentNode;
    }
  }
}

function laodSearchIndex(){
    if(typeof search_index !== "undefined"){
        var index = elasticlunr.Index.load(search_index);

        var searchCtrl = $("#search-input").val();

        if (searchCtrl.length > 0) {
          var searchResult = do_search(index, searchCtrl);
        }

        $("#search-input").keyup(function () {
          var searchCtrl = $("#search-input").val();
          var searchResult = do_search(index, searchCtrl);

          if (searchResult.length > 0) {
            $('#search-results').slideDown();
          } else {
            $('#search-results').slideUp();
          }
        });
    }
    else{
        setTimeout(laodSearchIndex, 250);
    }
}

window.onload = function () {
  if (window.outerWidth > 768) {
    $("#main-nav > ul").slideDown();
  } else {
    $("#main-nav > ul").slideUp();
  }

  elasticlunr.tokenizer.setSeperator(/[\s\-\.\\\/:,;]+/);

  if (document.documentElement.lang != 'en') {
    elasticlunr.multiLanguage('en', document.documentElement.lang);
  }

  laodSearchIndex();

  if(sessionStorage != undefined){
    var s = sessionStorage.getItem("search-input") || "";
    $('#search-input').val(s);

    if (s.length >= 3) {
      var context = document.querySelector("#content");
      var instance = new Mark(context);
      instance.mark(s);
    }
  }

  $('#search-input').on('input', function (e) {
    var context = document.querySelector("#content");
    var instance = new Mark(context);
    instance.unmark();

    if (e.currentTarget.value.length >= 3) {
      if(sessionStorage != undefined){
        sessionStorage.setItem("search-input", e.currentTarget.value);
      }
      var options = {
        "separateWordSearch": "false"
      };
      instance.mark(e.currentTarget.value, options);
    }else{
      if(sessionStorage != undefined){
        sessionStorage.removeItem("search-input");
      }
    }
  });
  $('#search > form').on('reset', function (e) {
    if(sessionStorage != undefined){
      sessionStorage.removeItem("search-input");
      var context = document.querySelector("#content");
      var instance = new Mark(context);
      instance.unmark();
    }
  });
};

$(document).ready(function () {
  $('#search a').mousedown(function (e) {
    e.preventDefault();
    var href = $(this).attr('href');
    $("#search-results").slideUp(function () {
      window.location = href;
    });
  });
  $("#search-input").focusout(function () {
    $('#search-results').slideUp();
  });
  $("#search-input").focusin(function () {
    if ($('#search-results').hasClass('is-active')) {
      $('#search-results').slideDown();
    }
  });
  $(window).resize(function () {
    if ($(window).width() >= 768) {
      $("#main-nav > ul").slideDown();
    }
  });
  var lastId,
      topMenu = $("#top-header"),
      topMenuHeight = topMenu.outerHeight(),
      mainNav = $("#main-nav"),
      menuHeight = topMenuHeight + mainNav.outerHeight() + 1;
  $('#main-nav a').each(function () {
    var this_page = $(this).attr('href').split("/").slice(-1)[0];

    if (location.pathname.split("/").slice(-1).indexOf(this_page) >= 0) {
      var node = $(this);

      while (node.length > 0 && !node.is("#main-nav")) {
        node = node.parent();
        if (node.is("li")) node.addClass("current_page");
      }
    }
  });
  $('#toc a').each(function () {
    $(this).click(function (e) {
      if (0 > e.offsetX) {
        $(this).parent().toggleClass("forceopen");
        return false;
      }
    });
  });
  var tocItems = _toConsumableArray(document.querySelectorAll('#toc a'));
  var tocHeadings = tocItems.map(function (tocItem) {
    var id = tocItem.getAttribute('href').substr(1);
    return document.getElementById(decodeURI(id)).parentNode;
  });

  if ("IntersectionObserver" in window) {
    var config = {
      root: null,
      rootMargin: '-50px 0px -60% 0px'
    };
    var tocObserver = new IntersectionObserver(function (entries, observer) {

      tocItems.forEach(function (link) {
        link.classList.remove('active');
        link.parentNode.classList.remove('open');
      });

      entries.forEach(function (entry) {
        var href = "#".concat(entry.target.querySelector("h1,h2,h3,h4,h5,h6,h7,h8").getAttribute('id'));
        var tocItem = tocItems.find(function (l) {
          return decodeURI(l.getAttribute('href')) === href;
        });

        if (entry.isIntersecting) {
          tocItem.classList.add('is-visible');
        } else {
          tocItem.classList.remove('is-visible');
        }
      });

      highlightFirstActive();
    }, config);
    tocHeadings.forEach(function (tocHeading) {
      tocObserver.observe(tocHeading);
    });
  } else {
    var toc = $("#toc"); // All list items

    var menuItems = toc.find("a"), // Anchors corresponding to menu items
    scrollItems = menuItems.map(function () {
      var item = $(decodeURI($(this).attr("href")));

      if (item.length) {
        return item;
      }
    }); // Bind to scroll

    $(window).scroll(function () {
      if ($(window).width() < 768) {
        menuItems.removeClass("active");
        return;
      } // Get container scroll position


      var fromTop = $(this).scrollTop() + menuHeight; // Get id of current scroll item

      var cur;

      if ($(window).scrollTop() + $(window).height() > $(document).height() - $("#footer").outerHeight()) {
        cur = scrollItems[scrollItems.length - 1];
      } else {
        cur = scrollItems.map(function () {
          if ($(this).offset().top < fromTop) return this;
        });
        cur = cur[cur.length - 1];
      } // Get the id of the current element


      var id = cur && cur.length ? cur[0].id : "";

      if (lastId !== id) {
        lastId = id; // Set/remove active class

        menuItems.removeClass("active").filter("[href='#" + encodeURI(id) + "']").addClass("active");
        menuItems.parent().removeClass("open");
        var node = menuItems.parent().end().filter("[href='#" + encodeURI(id) + "']").parent().addClass("open");

        while (node.length > 0 && !node.hasClass("sectlevel1")) {
          var parent = node.parent();
          if (node.parent().prop("nodeName") == "LI") parent.addClass("open");
          node = parent;
        }
      }
    });
  }
});
