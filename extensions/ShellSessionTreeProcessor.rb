require 'asciidoctor/extensions'
require 'cgi'

include Asciidoctor

Extensions.register do
  treeprocessor ShellSessionTreeProcessor
  treeprocessor ShellSessionPdfTreeProcessor
  docinfo_processor ShellSessionDocinfoProcessor
end

class ShellSessionDocinfoProcessor < Extensions::DocinfoProcessor
  def process(document)
    return nil unless document.attributes['backend'] =~ /^html/

    logger = ::Asciidoctor::LoggerManager.logger

    extdir = ::File.join(::File.dirname(__FILE__), 'ShellSession')
    stylesheet_name = 'shell_session.css'
    js_name = 'ShellSession.js'

    output = []

    if document.attr? 'linkcss'
      stylesheet_href = handle_stylesheet document, extdir, stylesheet_name
      output << [%(<link rel="stylesheet" href="#{stylesheet_href}">)]
    else
      @@stylesheet_content ||= document.read_asset(%(#{extdir}/#{stylesheet_name})).chomp
      output << ['<style>', @@stylesheet_content, '</style>']
    end

    logger.debug("ShellSessionDocinfoProcessor link js #{document.attr? 'linkjs'}")

    if document.attr? 'linkjs'
      js_href = handle_js document, extdir, js_name
      output << ["<script src='#{js_href}'/>"]
    else
      logger.debug('Compiling ShellSession.js') unless defined?(@@js_min_script)
      @@js_min_script ||= `closure-compiler --js=#{extdir}/#{js_name}`.strip.chomp
      output << ['<script type="text/javascript">', @@js_min_script, '</script>']
    end

    output * "\n"
  end

  def handle_stylesheet(doc, extdir, stylesheet_name)
    outdir = doc.attr?('outdir') ? doc.attr('outdir') : doc.attr('docdir')
    stylesoutdir = doc.normalize_system_path((doc.attr 'stylesdir'), outdir, (doc.safe >= SafeMode::SAFE ? outdir : nil))
    if stylesoutdir != extdir && doc.safe < SafeMode::SECURE && (doc.attr? 'copycss')
      destination = doc.normalize_system_path stylesheet_name, stylesoutdir, (doc.safe >= SafeMode::SAFE ? outdir : nil)
      create_path(destination)
      content = doc.read_asset %(#{extdir}/#{stylesheet_name})
      ::File.open(destination, 'w') { |f| f.write content }
    end
    File.join(((doc.attr 'stylesdir') || ''), stylesheet_name)
  end

  def handle_js(doc, extdir, js_name)
    outdir = doc.attr?('outdir') ? doc.attr('outdir') : doc.attr('docdir')
    jsoutdir = doc.normalize_system_path((doc.attr 'jsdir'), outdir, (doc.safe >= SafeMode::SAFE ? outdir : nil))
    if jsoutdir != extdir && doc.safe < SafeMode::SECURE && (doc.attr? 'copyjs')
      destination = doc.normalize_system_path js_name, jsoutdir, (doc.safe >= SafeMode::SAFE ? outdir : nil)
      create_path(destination)
      content = doc.read_asset %(#{extdir}/#{js_name})
      ::File.open(destination, 'w') { |f| f.write content }
    end
    File.join(((doc.attr 'jsdir') || ''), js_name)
  end

  def create_path(some_path)
    dirname = File.dirname(some_path)
    FileUtils.mkdir_p(dirname) unless File.directory?(dirname)
  end
end

# A treeprocessor extension that identifies literal blocks that appear to be
# commands in a shell session and converts them to listing blocks with a
# terminal style.
class ShellSessionTreeProcessor < Extensions::TreeProcessor
  LF = "\n".freeze

  def process(document)
    return nil unless document.attributes['backend'] =~ /^html/

    logger = ::Asciidoctor::LoggerManager.logger

    logger.debug('ShellSession html')

    (document.find_by(context: :literal) { |candidate| candidate.lines[0].start_with? '$ ', '> ', '# ' }).each do |block|
      (children = block.parent.blocks)[children.index block] = convert_to_terminal_listing block
    end

    nil
  end

  def convert_to_terminal_listing(block)
    attrs = block.attributes
    attrs['role'] = 'terminal'
    prompt_attr = attrs.key?('prompt') ? %( data-prompt="#{block.sub_specialchars attrs['prompt']}") : nil

    if attrs['subs'] && attrs['subs'].include?('attributes')
      attrs['subs'] = 'attributes'
    else
      attrs.delete('subs')
    end

    copy_text = []

    lines = (block.content.split LF).map do |line|
      if line.start_with?('$')
        l = (line[2..-1] || '').strip
        copy_text << l
        %(<span class="command"#{prompt_attr}>#{l}</span>)
      elsif line.start_with?('&gt;', '>')
        %(<span class="output">#{(line[5..-1] || '').lstrip}</span>)
      elsif line.start_with?('#')
        %(<span class="comment">#{line}</span>)
      else
        line
      end
    end

    lines << %(<i onclick="copyToClipboard(escape('#{copy_text * '\r\n'}'))" class="fa fa-clipboard clipboard" aria-hidden="true"></i>) unless copy_text.empty?

    new_block = create_listing_block block.parent, lines * LF, attrs, subs: attrs['subs']
    new_block.title = attrs['title']
    new_block
  end
end

# A treeprocessor extension that identifies literal blocks that appear to be
# commands in a shell session and converts them to listing blocks with a
# terminal style.
class ShellSessionPdfTreeProcessor < Extensions::TreeProcessor
  LF = "\n".freeze

  def process(document)
    return nil unless document.attributes['backend'] =~ /^pdf/

    logger = ::Asciidoctor::LoggerManager.logger

    logger.debug('ShellSession pdf')

    (document.find_by(context: :literal) { |candidate| candidate.lines[0].start_with? '$ ', '> ', '# ', '&gt; ' }).each do |block|
      (children = block.parent.blocks)[children.index block] = convert_to_terminal_listing block
    end

    nil
  end

  def convert_to_terminal_listing(block)
    attrs = block.attributes
    attrs['role'] = 'terminal'
    prompt_attr = attrs.key?('prompt') ? block.sub_specialchars(attrs['prompt']) : %($)

    attrs['subs'] = 'attributes' if attrs['subs'] && attrs['subs'].include?('attributes')

    lines = (block.content.split LF).map do |line|
      if line.start_with?('$')
        l = (line[2..-1] || '').strip
        %(#{prompt_attr} #{l})
      elsif line.start_with?('&gt;', '>')
        (line[5..-1] || '').lstrip
      elsif line.start_with?('#')
        line
      else
        line
      end
    end

    new_block = create_listing_block block.parent, lines * LF, attrs, subs: attrs['subs']
    new_block.title = attrs['title']
    new_block
  end
end
