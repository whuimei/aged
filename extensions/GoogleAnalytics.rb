require 'asciidoctor/extensions'

Asciidoctor::Extensions.register do
  if @document.basebackend? 'html'
    docinfo_processor do
      at_location :head
      process do |doc|
        next unless (ga_account_id = doc.attr 'google-analytics-account')
        %(<!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=#{ga_account_id}"></script>
        <script>
        "use strict";

        window.dataLayer = window.dataLayer || [];

        function gtag() {
        dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', '#{ga_account_id}');
        </script>)
      end
    end
  end
end
