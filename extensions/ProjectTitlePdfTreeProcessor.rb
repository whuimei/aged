require 'asciidoctor/extensions'
require 'yaml'

class ProjectTitlePdfTreeProcessor < Asciidoctor::Extensions::Treeprocessor
  def process(document)
    cnf = YAML.load_file('/aged/project/.aged.yml')
    title = ''
    cnf['project'].each do |p|
      if p['pdf_title']
        p['name'] = p['name'][document.attributes['lang']] unless p['name'].is_a?(String)
        title = [title, p['name']].join(' ')
      end
    end
    unless title.empty?
      document.find_by(context: :section) { |section| section.title = "#{section.title} : #{title}" if section.level.zero? }
    end
    nil
  end
end
