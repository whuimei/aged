require 'asciidoctor'
require 'asciidoctor/extensions'
require 'nokogiri'

Asciidoctor::Extensions.register do
  postprocessor LazyLoadImagesPostprocessor
  docinfo_processor LazyLoadImagesDocinfoProcessor
end

class LazyLoadImagesDocinfoProcessor < Asciidoctor::Extensions::DocinfoProcessor
  def process document
    return nil unless document.attributes['backend'] =~ /^html/

    logger = ::Asciidoctor::LoggerManager.logger

    extdir = ::File.join(::File.dirname(__FILE__), 'LazyLoadImages')
    js_name = 'LazyLoadImages.js'

    output = []

    if document.attr? 'linkjs'
      js_href = handle_js document, extdir, js_name
      output << ["<script src=#{js_href}/>"]
    else
      logger.debug('Compiling LazyLoadImages.js') unless defined?(@@js_min_script)
      @@js_min_script ||= `closure-compiler --js=#{extdir}/#{js_name}`.strip.chomp
      output << ['<script type="text/javascript">', @@js_min_script, '</script>']
    end

    output * "\n"
  end

  def handle_js(doc, extdir, js_name)
    outdir = ''
    outdir = doc.attr('outdir') if doc.attr?('outdir')
    jsoutdir = doc.normalize_system_path((doc.attr 'jsdir'), outdir, (doc.safe >= SafeMode::SAFE ? outdir : nil))
    if jsoutdir != extdir && doc.safe < SafeMode::SECURE && (doc.attr? 'copyjs')
      destination = doc.normalize_system_path js_name, jsoutdir, (doc.safe >= SafeMode::SAFE ? outdir : nil)
      create_path(destination)
      content = doc.read_asset %(#{extdir}/#{js_name})
      ::File.open(destination, 'w') { |f| f.write content }
      destination
    end
    File.join(((doc.attr 'jsdir') || ''), js_name)
  end

  def create_path(some_path)
    dirname = File.dirname(some_path)
    FileUtils.mkdir_p(dirname) unless File.directory?(dirname)
  end
end

class LazyLoadImagesPostprocessor < Asciidoctor::Extensions::Postprocessor
  def process(document, output)
    return output unless document.attributes['backend'] =~ /^html/

    logger = ::Asciidoctor::LoggerManager.logger

    doc = Nokogiri::HTML::Document.parse(output)

    p_threads = []
    doc.xpath('//img').each do |img|
      next unless img['src'].end_with?('.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG')

      image_dir = image_output_dir(document)
      ext = File.extname(img['src'])

      img_dir = document.normalize_system_path('..', '', (document.safe >= SafeMode::SAFE ? '' : nil))
      image_file = File.join(img_dir, img['src'])

      img['class'] = 'lazy'
      img['data-src'] = img['src']
      image_file_name_low = File.basename(img['src'], ext) + '_low.jpg'

      image_file_low = document.normalize_system_path image_file_name_low, image_dir

      unless File.exist?(image_file_low)
        logger.info("Creating low res version of #{image_file}")
        cmd = %(convert -strip -interlace Plane -resize 25% -gaussian-blur 0x10 -resize 400% -quality 15% #{image_file} #{image_file_low})
        logger.debug(cmd)
        p_threads << Process.detach(Process.spawn(cmd))
      end

      img['src'] = File.join(File.dirname(img['src']), image_file_name_low)
    end

    p_threads.each(&:join)

    doc.to_html
  end

  def image_output_dir(parent)
    document = parent.document

    images_dir = parent.attr('imagesoutdir', nil, true)

    if images_dir
      base_dir = nil
    else
      base_dir = parent.attr('outdir', nil, true) || doc_option(document, :to_dir)
      images_dir = parent.attr('imagesdir', nil, true)
    end

    parent.normalize_system_path(images_dir, base_dir)
  end

  def doc_option(document, key)
    value = document.options[key] if document.respond_to?(:options)

    if document.nested? && value.nil?
      doc_option(document.parent_document, key)
    else
      value
    end
  end
end
