require 'asciidoctor'
require 'asciidoctor/extensions'
require 'yaml'
require 'nokogiri'
require 'shellwords'
require 'erb'
require 'ostruct'

module GluePath
  def self.join(*paths, separator: '/')
    paths = paths.compact.reject(&:empty?)
    last = paths.length - 1
    paths.each_with_index.map { |path, index| _expand(path, index, last, separator) }.join
  end

  def self._expand(path, current, last, separator)
    path = path[1..-1] if path.start_with?(separator) && current != 0

    path = [path, separator] unless path.end_with?(separator) || current == last

    path
  end
end

class TopNavbarDocinfoProcessor < Asciidoctor::Extensions::DocinfoProcessor
  def process(document)
    logger = ::Asciidoctor::LoggerManager.logger

    outfile = document.attributes['docname']
    lang = ''
    lang = Regexp.last_match(1) if outfile =~ /.*_(\w{2})/

    logger.debug('Compiling TopNavbar.js')

    extdir = ::File.join(::File.dirname(__FILE__), 'TopNavbar')
    js_name = 'TopNavbar.js'

    output = []

    if document.attr? 'linkjs'
      js_href = handle_js document, extdir, js_name
      output << ["<script src='#{js_href}'/>"]
    else
      @@js_min_script ||= `closure-compiler --js=#{extdir}/#{js_name}`.strip.chomp
      output << ['<script type="text/javascript">', @@js_min_script, '</script>']
    end

    rel_topdir = document.attributes['rel_topdir']

    if lang != 'en'
      output << ["<script async type='text/javascript' src='#{rel_topdir}js/lunr.stemmer.support.min.js'/>"]
      output << ["<script async type='text/javascript' src='#{rel_topdir}js/lunr.multi.min.js'/>"]
      output << ["<script async type='text/javascript' src='#{rel_topdir}js/lunr.#{lang}.min.js'/>"]
    end
    output << ["<script async type='text/javascript' src='js/index_#{lang}.js'/>"]

    output * "\n"
  end

  def handle_js(doc, extdir, js_name)
    outdir = doc.attr?('outdir') ? doc.attr('outdir') : doc.attr('docdir')
    jsoutdir = doc.normalize_system_path((doc.attr 'jsdir'), outdir, (doc.safe >= SafeMode::SAFE ? outdir : nil))
    if jsoutdir != extdir && doc.safe < SafeMode::SECURE && (doc.attr? 'copyjs')
      destination = doc.normalize_system_path js_name, jsoutdir, (doc.safe >= SafeMode::SAFE ? outdir : nil)
      create_path(destination)
      content = doc.read_asset %(#{extdir}/#{js_name})
      ::File.open(destination, 'w') { |f| f.write content }
      destination
    end
    File.join(((doc.attr 'jsdir') || ''), js_name)
  end

  def create_path(some_path)
    dirname = File.dirname(some_path)
    FileUtils.mkdir_p(dirname) unless File.directory?(dirname)
  end
end

class TopNavbarPostprocessor < Asciidoctor::Extensions::Postprocessor
  def process(document, output)
    logger = ::Asciidoctor::LoggerManager.logger

    # outfile = document.attributes["outfile"]
    outfile = document.attributes['docfile']
    outfile = File.basename(outfile)
    rel_topdir = document.attributes['rel_topdir']
    lang = ''
    if outfile =~ /(.*)_(\w{2})\.(?:html|adoc)/
      outfile = Regexp.last_match(1)
      lang = Regexp.last_match(2)
    end

    document.attributes['lang'] = lang if document.attributes['lang'].nil?
    lang = document.attributes['lang'] if lang.empty?

    if document.attributes['lang'] != lang
      logger.error(':lang: equal to the file suffix _xx where xx is the language code')
      raise "Language miss match #{document.attributes['lang']} != #{lang} in #{document.attributes['docfile']}"
    end

    cnf = YAML.load_file('/aged/project/.aged.yml')
    @@lang_config ||= YAML.load_file('/aged/extensions/TopNavbar/iso_639-1.yml')

    doc = Nokogiri::HTML::Document.parse(output)
    body = doc.at_css 'body'
    header = doc.at_css 'div#header'
    toc = doc.at_css 'div#toc'
    content = doc.at_css 'div#content'

    revnumber = doc.at_css 'span#revnumber'

    languages = []
    if !cnf['languages'].nil? && cnf['languages'].length > 1
      cnf['languages'].each do |t|
        transl_lang, _country = t.split('_')
        next if lang == transl_lang
        /^(?<lang_name>[^,(]*)/ =~ @@lang_config[transl_lang]['nativeName']
        lang_name.capitalize!
        if File.exist? "#{document.base_dir}/#{outfile}_#{transl_lang}.adoc"
          target = "#{outfile}_#{transl_lang}.html"
        elsif !cnf['languages_defaultpage'].nil?
          target = "#{cnf['languages_defaultpage']}_#{transl_lang}.html"
        else
          next
        end
        languages.push(href: target, name: lang_name)
      end
    end

    @@template ||= ERB.new(File.read('extensions/TopNavbar/top-header.html.erb'), trim_mode: '-')
    namespace = OpenStruct.new
    namespace.logo = cnf['logo']
    unless namespace.logo.nil?
      default_logo_href = namespace.logo['href'].values.first unless namespace.logo['href'].is_a?(String)
      namespace.logo['href'] = namespace.logo['href'][lang] unless namespace.logo['href'].is_a?(String)
      namespace.logo['href'] = default_logo_href if namespace.logo['href'].nil?
    end
    namespace.projects = cnf['project']
    namespace.projects.each_with_index do |project, index|
      if project['name'].is_a?(Hash)
        default_name = project['name'].values.first
        project['name'] = project['name'][lang]
        project['name'] = default_name if project['name'].nil?
      end
      if project['href'].is_a?(Hash)
        default_href = project['href'].values.first
        project['href'] = project['href'][lang]
        project['href'] = default_href if project['href'].nil?
      end
    end
    namespace.languages = languages
    namespace.git = nil
    namespace.rel_topdir = rel_topdir
    namespace.pdf_file = document.attributes['pdf_file']

    if cnf['git']
      git = OpenStruct.new
      if File.exist? "#{document.base_dir}/#{document.attributes['docname']}.adoc"
        git.improve = GluePath.join(cnf['git'], 'blob', document.attributes['revnumber'] || 'master', 'docs', document.attributes['docname']) + '.adoc'
      end
      git.issue = GluePath.join(cnf['git'], 'issues', 'new')
      namespace.git = git
    end

    maintitle = @@template.result_with_hash(namespace.to_h)

    unless cnf['versions'].nil? || revnumber.nil?
      versions = ''
      cnf['versions'].each_with_index do |v, i|
        ver = version(v)
        versions += if i.zero?
                      %(<a href='#{rel_topdir}index_#{lang}.html'>#{ver[:version_name]}</a>)
                    else
                      %(<a href='#{rel_topdir}#{ver[:branch]}/index_#{lang}.html'>#{ver[:version_name]}</a>)
                    end
      end
      versions = %(<div class='dropdown-content'>#{versions}</div>)

      versions = revnumber.add_next_sibling versions
    end

    body.prepend_child content
    body.prepend_child toc unless toc.nil?
    body.prepend_child header
    # body.prepend_child mainnav
    body.prepend_child maintitle

    doc.to_html
  end

  def version(version)
    return nil unless version

    l = version.split(';')
    bransh = l[0]
    version_name = if l.length == 2
                     l[1]
                   else
                     l[0]
                   end
    { branch: bransh.strip, version_name: version_name.strip }
  end
end
