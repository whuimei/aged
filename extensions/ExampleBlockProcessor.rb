require 'asciidoctor/extensions'

include Asciidoctor

# An extension that introduces a custom admonition type, complete
# with a custom icon.
#
# Usage
#
#   [QUESTION]
#   ====
#   What's the main tool for selecting colors?
#   ====
#
# or
#
#   [QUESTION]
#   What's the main tool for selecting colors?
#

Extensions.register do
  block QuestionExampleBlockProcessor
  block GoodExampleBlockProcessor
  block BadExampleBlockProcessor
end

class QuestionExampleBlockProcessor < Extensions::BlockProcessor
  use_dsl
  named :QUESTION
  on_contexts :example, :paragraph

  def process(parent, reader, attrs)
    attrs['name'] = 'question'
    attrs['Caption'] = 'Question'
    create_block parent, :admonition, reader.lines, attrs, content_model: :compound
  end
end

class GoodExampleBlockProcessor < Extensions::BlockProcessor
  use_dsl
  named :GOOD
  on_contexts :example, :paragraph

  def process(parent, reader, attrs)
    attrs['name'] = 'good'
    attrs['Caption'] = 'Good'
    create_block parent, :admonition, reader.lines, attrs, content_model: :compound
  end
end

class BadExampleBlockProcessor < Extensions::BlockProcessor
  use_dsl
  named :BAD
  on_contexts :example, :paragraph

  def process(parent, reader, attrs)
    attrs['name'] = 'bad'
    attrs['Caption'] = 'Bad'
    create_block parent, :admonition, reader.lines, attrs, content_model: :compound
  end
end
