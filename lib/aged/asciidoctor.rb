require 'logger'
require 'asciidoctor'
require 'asciidoctor-pdf'
require 'asciidoctor-interdoc-reftext'
require 'nokogiri'
require '/aged/extensions/TopNavbar.rb'
require '/aged/extensions/ProjectTitlePdfTreeProcessor.rb'

class PdfGroup < Asciidoctor::Extensions::Group
  def activate(registry)
    registry.treeprocessor ProjectTitlePdfTreeProcessor
  end
end

class TopNavGroup < Asciidoctor::Extensions::Group
  def activate(registry)
    registry.postprocessor TopNavbarPostprocessor
    registry.docinfo_processor TopNavbarDocinfoProcessor
  end
end

module Aged
  class Asciidoctor
    def self.build_navigation(navs_dir, lang, attributes)
      ::Aged.logger.debug('build navigation')
      attrs = []
      attributes.each do |key, val|
        attrs << if val.to_s.empty?
                   key
                 else
                   "#{key}=#{val}"
                 end
      end
      nav = ::Asciidoctor.load_file File.join(navs_dir, "navigation_#{lang}.adoc"), to_file: false, safe: :unsafe, header_footer: false, attributes: attrs.flatten
      nav_html = nav.convert.lines.reject { |e| e =~ /.*div.*/ }.join
      ::Aged.logger.debug("nav html #{nav_html}")
      nav_html
    end

    attr_accessor :requires_html
    attr_accessor :attributes_html
    attr_accessor :requires_pdf
    attr_accessor :attributes_pdf
    attr_accessor :build_dir
    attr_accessor :revnumber
    attr_accessor :build_html
    attr_accessor :build_pdf
    attr_accessor :adoc_file

    def initialize(adoc_file, executer, build_dir: './', build_html: true, build_pdf: false, nav: {})
      ::Asciidoctor::LoggerManager.logger = ::Aged.logger

      @adoc_file = adoc_file
      @executer = executer
      @build_dir = build_dir
      @requires_html = []
      @attributes_html = {}
      @requires_pdf = []
      @attributes_pdf = []
      @build_pdf = build_pdf
      @build_html = build_html
      @nav = nav
    end

    def build(include_nav: true)
      ::Aged.logger.debug("asciidoctor build file #{@adoc_file}")
      build_html(include_nav) if @build_html
      build_pdf if @build_pdf
    end

    def build_html(include_nav)
      ::Aged.logger.info("generating html #{@adoc_file}")
      target_file = File.join(@build_dir, "#{File.basename(@adoc_file, '.*')}.html")

      lang = ''
      lang = Regexp.last_match(1) if target_file =~ /.*_(\w{2})\.(?:html|adoc)/

      if lang.empty?
        IO.foreach(@adoc_file) do |l|
          lang = Regexp.last_match(1) if l =~ /:lang:\s+(\w{2})/
        end
        target_file = File.join(@build_dir, "#{File.basename(@adoc_file, '.*')}_#{lang}.html") unless lang.empty?
      end

      if lang.empty?
        ::Aged.logger.error("The file #{@adoc_file} is missing language code the file name must end with _xx or cantain :lang: xx where xx is the language code")
        raise 'Language is missing'
      end

      @attributes_html['pdf_file'] = "#{File.basename(@adoc_file, '.*')}.pdf" if @build_pdf

      @requires_html.flatten.map do |r|
        ::Aged.logger.debug("file #{@adoc_file} require #{r}")
        require r
      end

      if include_nav
        ::Aged.logger.debug('Register TopNavGroup')
        ::Asciidoctor::Extensions.register :top_nav_bar, TopNavGroup
      end

      attributes = []
      attributes << 'projdir=/aged/project/'
      @attributes_html.each do |key, val|
        ::Aged.logger.debug("attribute key #{key} val #{val}")
        if @attributes_html.key?('rel_topdir') && !@attributes_html['rel_topdir'].empty?
          val = "#{@attributes_html['rel_topdir']}#{val[2..-1]}" if val.nil? == false && val[0, 2] == './'
        end
        attributes << if val.to_s.empty?
                        key
                      else
                        "#{key}=#{val}"
                      end
      end

      ::Aged.logger.debug("build html file #{target_file} args #{attributes.flatten}")

      adoc = ::Asciidoctor.load_file @adoc_file, mkdirs: true, to_file: false, safe: :unsafe, header_footer: true, attributes: attributes.flatten

      doc = Nokogiri::HTML::Document.parse(adoc.convert)

      if @nav && include_nav
        template = ERB.new(File.read('lib/aged/templates/main-nav.html.erb'), trim_mode: '-')
        namespace = OpenStruct.new(lang: lang, nav: @nav[lang])
        mainnav = template.result_with_hash(namespace.to_h)

        top_header = doc.at_css('#top-header')
        top_header.add_next_sibling(mainnav)
      end

      tables = doc.xpath("//div[contains(@class, 'sect') or @class='content']/table")
      tables.each do |table|
        if table.key?('id')
          table.wrap("<div id='#{table['id']}' class='table'>")
          table.delete('id')
        else
          table.wrap("<div class='table'>")
        end
      end

      details = doc.xpath("//details")
      details.each do |detail|
        if detail.key?('id')
          detail.wrap("<div id='#{detail['id']}' class='details'>")
          detail.delete('id')
        else
          detail.wrap("<div class='details'>")
        end
      end

      File.open(target_file, 'w') do |f|
        f.puts doc.to_html
      end

      return unless include_nav

      ::Aged.logger.debug('Unregister TopNavGroup')
      ::Asciidoctor::Extensions.unregister :top_nav_bar
    end

    def build_pdf()
      ::Aged.logger.info("generating pdf #{@adoc_file}")
      target_file = File.join(@build_dir, "#{File.basename(@adoc_file, '.*')}.pdf")

      ::Aged.logger.debug("build pdf file #{target_file} args #{@attributes_pdf.flatten}")

      @requires_pdf.flatten.map do |r|
        ::Aged.logger.debug("require file #{r}")
        require r
      end

      @attributes_pdf << 'projdir=/aged/project/'
      @attributes_pdf << 'scripts=cjk'

      ::Asciidoctor::Extensions.register :project_title_pdf, PdfGroup

      ::Asciidoctor.convert_file(@adoc_file,
                                 backend: :pdf,
                                 mkdirs: true,
                                 to_dir: @build_dir,
                                 to_file: target_file,
                                 safe: :unsafe,
                                 header_footer: true,
                                 attributes: @attributes_pdf.flatten)

      ::Asciidoctor::Extensions.unregister :project_title_pdf
    end
  end
end
