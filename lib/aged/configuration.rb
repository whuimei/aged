require 'yaml'

module Aged
  class Configuration
    attr_accessor :project_dir, :build_dir
    attr_writer

    def initialize
      @project_dir = '.'
    end

    def load_yaml(file)
      @cnf = YAML.load_file(file)
      validate
    end

    def config
      @cnf
    end

    def config=(config)
      @cnf = config
      validate
    end

    def validate
      raise 'Config empty' unless @cnf
      raise 'Config missing project' if @cnf['project'].nil?
      raise 'Config missing start_page' if @cnf['start_page'].nil?
      raise 'Config missing languages' if @cnf['languages'].nil?
      raise 'Config languages must be an array with atleast one language' unless @cnf['languages'].is_a?(Array) && @cnf['languages'].length >= 1
    end

    def project
      @cnf['project']
    end

    def asciidoc_to_html
      return Dir.glob("#{docs_dir()}/*.adoc") if @cnf['to_html'].nil?
      if @cnf['to_html'].is_a?(Array)
        @cnf['to_html'].map { |n| Dir.glob("#{docs_dir}/#{n}") }
      else
        return Dir.glob("#{docs_dir}/#{@cnf['to_html']}")
      end
    end

    def asciidoc_to_pdf
      return [] if @cnf['to_pdf'].nil?
      if @cnf['to_pdf'].kind_of?(Array)
        @cnf['to_pdf'].map { |n| Dir.glob("#{docs_dir()}/#{n}") }.flatten
      else
        return Dir.glob("#{docs_dir()}/#{@cnf['to_pdf']}")
      end
    end

    def docs_dir
      docs = @cnf['docs'] || "docs"
      return File.join(@project_dir,docs)
    end

    def navs_dir
      return File.join(@project_dir,@cnf['nav_dir'] || "")
    end

    def start_page
      return @cnf['start_page']
    end

    def start_page=(start_page)
      @cnf['start_page'] = start_page
    end

    def css
      return @cnf['css'] || "aged.min.css"
    end

    def css_single
      return "" if @cnf['single_page'].nil?
      return @cnf['single_page']['css'] || ""
    end

    def languages
      return @cnf['languages'].map { |n| n.split('_')[0] }
    end

    def countries
      return @cnf['languages'].map { |n| n.split('_')[1] }
    end

    def have_translations
      return !@cnf['pot'].nil? && File.exist?(File.join(project_dir, translations_dir()))
    end

    def translations_dir
      return "po" if @cnf['pot'].nil?
      return @cnf['pot']['dir'] || "po"
    end

    def translations_min_level
      return 80 if @cnf['pot'].nil?
      return @cnf['pot']['min_level'] || 80
    end

    def translations_files
      if @cnf['pot'].nil? || @cnf['pot']['files'].nil?
        return Dir.glob(File.join(docs_dir(),"/**/*.adoc"))
      else
        return @cnf['pot']['files'].map { |file| Dir.glob(File.join(project_dir(),file)) }.flatten(1)
      end
    end

    def requires
      return @cnf['requires'] || []
    end

    def attributes
      return @cnf['attributes'] || {}
    end

    def get_attribute(attribute)
      return @cnf['attributes'][attribute]
    end

    def set_attribute(attribute, value)
      @cnf['attributes'][attribute] = value
    end

    def version
      return @version
    end

    def version=(version)
      @version = version
    end

    def versions
      return @cnf['versions']
    end

    def single_page_template
      return nil if @cnf['single_page'].nil?
      return nil if @cnf['single_page']['template'].nil?
      return File.join(project_dir(),@cnf['single_page']['template'])
    end

    def single_page_html
      return false if @cnf['single_page'].nil?
      return false if @cnf['single_page']['format'].nil?
      return @cnf['single_page']['format'].include? 'html'
    end

    def single_page_pdf
      return false if @cnf['single_page'].nil?
      return false if @cnf['single_page']['format'].nil?
      return @cnf['single_page']['format'].include? 'pdf'
    end

    def single_page_embed_images
      return false if @cnf['single_page'].nil?
      return false if @cnf['single_page']['embed_images'].nil?
      return @cnf['single_page']['embed_images']
    end

    def single_page_css
      return nil if @cnf['single_page'].nil?
      return nil if @cnf['single_page']['css'].nil?
      return @cnf['single_page']['css']
    end

    def copy
      return [] if @cnf['copy'].nil?
      return @cnf['copy']
    end

    def threads
      return 5 if (@cnf['threads'].nil? || @cnf['threads'] < 1)
      return @cnf['threads']
    end

    def image_max_size?
      return !@cnf['image_max_size'].nil?
    end

    def image_max_size
      return @cnf['image_max_size']
    end
  end
end
