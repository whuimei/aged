function _toConsumableArray(arr) {
  return (
    _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread()
  );
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

function _iterableToArray(iter) {
  if (
    Symbol.iterator in Object(iter) ||
    Object.prototype.toString.call(iter) === "[object Arguments]"
  )
    return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }
    return arr2;
  }
}

function openTarget() {
  var hash = location.hash.substring(1);
  if(hash) {
    var target_elem = document.getElementById(hash);
    elem = target_elem.firstChild;
    do { // go up till <html>
      if(elem && elem.tagName && elem.tagName.toLowerCase() === 'details') elem.open = true;
    } while(elem = elem.parentElement)
    target_elem.scrollIntoView();
  }
}

$(document).ready(function() {
  $(function() {
    openTarget();

    $("a[href^='#']").click(function(e) {
      openTarget();
    });

    if ("onhashchange" in window) {// does the browser support the hashchange event?
      window.onhashchange = function () {
        openTarget();
      }
    }

    tocItems = _toConsumableArray(document.querySelectorAll("#toc a"));
    tocHeadings = tocItems.map(function(tocItem) {
      var id = tocItem.getAttribute("href").substr(1);
      return document.getElementById(decodeURI(id)).parentNode;
    });
    top_btn = $("#to-top-btn");
    footerHeight = $("#footer").outerHeight() + 20;

    if ("IntersectionObserver" in window) {
      var footerObserver = new IntersectionObserver(function(
        entries,
        observer
      ) {
        entries.forEach(function(entry) {
          if (entry.isIntersecting) {
            top_btn.css({
              bottom: "" + footerHeight + "px"
            });
          } else {
            top_btn.css({
              bottom: "20px"
            });
          }
        });
      });
      var headerObserver = new IntersectionObserver(function(
        entries,
        observer
      ) {
        entries.forEach(function(entry) {
          if (entry.isIntersecting) {
            top_btn.removeClass("visible");
            top_btn.addClass("hidden");
          } else {
            top_btn.css({
              bottom: "20px"
            });
            top_btn.removeClass("hidden");
            top_btn.addClass("visible");
          }
        });
      });
      footerObserver.observe(document.querySelector("#footer"));
      headerObserver.observe(document.querySelector("#top-header"));
    } else {
      // Add the sticky class to the button when reach its scroll position. Remove "sticky" when leave the scroll position
      var myFunction = function myFunction() {
        top_btn = $("#to-top-btn");

        if ($(window).width() < 768) {
          top_btn.removeClass("visible");
          top_btn.addClass("hidden");
          return;
        }

        if (window.pageYOffset >= 100) {
          top_btn.addClass("visible");
          top_btn.removeClass("hidden");
          distanceFromBottom = Math.floor(
            $(document).height() - $(document).scrollTop() - $(window).height()
          );
          footerHeight = $("#footer").outerHeight() + 20;

          if (distanceFromBottom < footerHeight) {
            top_btn.css({
              bottom: "" + footerHeight + "px"
            });
          } else {
            top_btn.css({
              bottom: "20px"
            });
          }
        } else {
          top_btn.removeClass("visible");
          top_btn.addClass("hidden");
        }
      }; // Get the offset position of the navbar

      if ($("#main-nav").length > 0) {
        $(window).scroll(myFunction);
        myFunction();
      }
    }
  });
});
