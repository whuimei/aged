require 'fileutils'
require 'date'
require 'open-uri'
require 'find'
require 'logger'
require 'English'

require 'aged/asciidoctor'
require 'aged/gettext'

module Aged
  AGED_VERSION = '1.0'.freeze
  FONT_AWESOME = 'font-awesome-4.7.0.min'.freeze

  # Building generate aged documentation
  class Aged
    @rel_topdir = ''

    EXECUTER = lambda do |cmd|
      ::Aged.logger.debug(cmd)
      ret = `#{cmd} 2>&1`
      if $CHILD_STATUS.exitstatus != 0
        ::Aged.logger.error(cmd)
        abort("Error: #{ret}")
      end
      return '' if ret.empty?

      ret.each_line do |line|
        if (%r{^\/usr\/lib\/ruby\/gems\/} =~ line).nil?
          ::Aged.logger.info(line)
        else
          ::Aged.logger.debug(line)
        end
      end

      return ret
    end

    def build_file(configuration, adoc_file)
      ::Aged.logger.debug("build file #{adoc_file}")
      @configuration = configuration

      ::Aged.logger.debug('build')
      @rel_topdir = if @configuration.version && !@configuration.version[:master]
                      '../'
                    else
                      ''
                    end

      setup_build

      nav = create_nav

      build_adoc_file(adoc_file, nav)

      FileUtils.rm_rf File.join(@configuration.build_dir, '.asciidoctor')

      build_to_output
    end

    def build(configuration)
      configuration.validate
      @configuration = configuration

      ::Aged.logger.debug('build')
      @rel_topdir = if @configuration.version && !@configuration.version[:master]
                      '../'
                    else
                      ''
                    end

      setup_build
      translate if @configuration.have_translations

      nav = create_nav

      generate_site(nav)

      build_index_for_languages

      build_sigle_page unless @configuration.single_page_template.nil?
      FileUtils.rm_rf File.join(@configuration.build_dir, '.asciidoctor')

      build_to_output
    end

    def build_index
      ::Aged.logger.debug('build_index')
      def_lang = @configuration.languages.first
      index = create_redirect_page("#{@configuration.start_page}_#{def_lang}.html", def_lang)
      File.open(File.join(@configuration.build_dir, 'index.html'), 'w') { |f| f.write index }

      @configuration.languages.each do |lang|
        index = create_redirect_page("#{@configuration.start_page}_#{lang}.html", lang)
        File.open(File.join(@configuration.build_dir, "index_#{lang}.html"), 'w') { |f| f.write index }
      end
    end

    def setup_font_awesome
      FileUtils.mkdir_p File.join(@configuration.build_dir, 'stylesheets')
      file = File.join(@configuration.build_dir, 'stylesheets', "#{FONT_AWESOME}.css")
      FileUtils.cp('/aged/css/font-awesome.min.css', file) unless File.exist?(file)

      FileUtils.mkdir_p File.join(@configuration.build_dir, 'fonts')
      file = File.join(@configuration.build_dir, 'fonts', 'fontawesome-webfont.woff2')
      FileUtils.cp('/aged/fonts/fontawesome-webfont.woff2', file) unless File.exist?(file)

      file = File.join(@configuration.build_dir, 'fonts', 'fontawesome-webfont.woff')
      FileUtils.cp('/aged/fonts/fontawesome-webfont.woff', file) unless File.exist?(file)

      file = File.join(@configuration.build_dir, 'fonts', 'fontawesome-webfont.svg')
      FileUtils.cp('/aged/fonts/fontawesome-webfont.svg', file) unless File.exist?(file)

      file = File.join(@configuration.build_dir, 'fonts', 'fontawesome-webfont.ttf')
      FileUtils.cp('/aged/fonts/fontawesome-webfont.ttf', file) unless File.exist?(file)
    end

    def setup_jquery
      FileUtils.mkdir_p File.join(@configuration.build_dir, 'js')
      file = File.join(@configuration.build_dir, 'js', 'jquery-3.3.1.min.js')
      FileUtils.cp('/aged/js/jquery-3.3.1.min.js', file) unless File.exist?(file)
    end

    def setup_js_mark
      FileUtils.mkdir_p File.join(@configuration.build_dir, 'js')
      file = File.join(@configuration.build_dir, 'js', 'jquery.mark.min.js')
      FileUtils.cp('/aged/js/jquery.mark.min.js', file) unless File.exist?(file)
    end

    def setup_js_polyfill
      FileUtils.mkdir_p File.join(@configuration.build_dir, 'js')
      file = File.join(@configuration.build_dir, 'js', 'polyfill.min.js')
      FileUtils.cp('/aged/js/polyfill.min.js', file) unless File.exist?(file)
    end

    def copy_maybe_min(from, to)
      return false unless File.exist?(to)
      if File.exist?(from)
        FileUtils.cp(from, to)
        return true
      elsif File.exist?(from.sub('.min', ''))
        FileUtils.cp(from.sub('.min', ''), to)
        return true
      end
    end

    def setup_js_elasticlunr
      FileUtils.mkdir_p File.join(@configuration.build_dir, 'js')
      file = File.join(@configuration.build_dir, 'js', 'elasticlunr.min.js')
      FileUtils.cp('/aged/js/elasticlunr.min.js', file) unless File.exist?(file)

      @configuration.languages.each do |lang|
        next unless lang != 'en'
        file = File.join(@configuration.build_dir, 'js', 'lunr.stemmer.support.min.js')
        copy_maybe_min(File.join('/', 'aged', 'lib', 'elasticlunr_lang', 'lunr.stemmer.support.min.js'), file)

        file = File.join(@configuration.build_dir, 'js', 'lunr.multi.min.js')
        copy_maybe_min(File.join('/', 'aged', 'lib', 'elasticlunr_lang', 'lunr.multi.min.js'), file)

        file = File.join(@configuration.build_dir, 'js', "lunr.#{lang}.min.js")
        unless copy_maybe_min(File.join('/', 'aged', 'project', 'locale', "lunr.#{lang}.min.js"), file)
          copy_maybe_min(File.join('/', 'aged', 'lib', 'elasticlunr_lang', "lunr.#{lang}.min.js"), file)
        end
      end
    end

    def create_redirect_page(index, lang)
      template = ERB.new(File.read(File.join(File.dirname(__FILE__), 'aged', 'index.html.erb')), trim_mode: '-')
      template.result_with_hash(index: index, lang: lang)
    end

    def translate
      ::Aged.logger.debug('translate')
      cfg_file = File.join(@configuration.project_dir, 'aged.cfg')
      rev = git_revision_string
      project_name = @configuration.project.map { |p| p['name'] }.join(' ')
      translation_dir = @configuration.translations_dir
      gettext = Gettext.new(@configuration.translations_files,
                            cfg_file,
                            EXECUTER,
                            translation_dir: translation_dir,
                            project_name: project_name,
                            rev: rev,
                            min_level: @configuration.translations_min_level)
      gettext.working_dir = @configuration.project_dir

      gettext.translate

      data_dir = File.join(@configuration.project_dir, 'data')
      FileUtils.mkdir_p(data_dir) unless File.exist?(data_dir)
      gettext.save_stats(File.join(data_dir, 'po_stats.dat'))
      gettext.save_translation_status(File.join(data_dir, 'translations.dat'))
    end

    def generate_site(nav)
      @configuration.asciidoc_to_html.each do |adoc_file|
        build_adoc_file(adoc_file, nav)
      end
    end

    def build_adoc_file(adoc_file, nav = {})
      ::Aged.logger.info("Building #{File.basename(adoc_file, '.*')}")
      asciidoctor = Asciidoctor.new(adoc_file, EXECUTER, build_dir: @configuration.build_dir, nav: nav)

      asciidoctor.requires_html << @configuration.requires
      asciidoctor.requires_pdf << @configuration.requires

      asciidoctor.attributes_html.merge!('nofooter' => '',
                                         'aged-version' => AGED_VERSION,
                                         'rel_topdir' => @rel_topdir,
                                         'imagesdir' => 'images',
                                         'docinfodir' => "#{@configuration.project_dir}/docinfo",
                                         'docinfo' => 'shared',
                                         'stylesheet' => File.basename(@configuration.css),
                                         'stylesdir' => "#{@rel_topdir}stylesheets",
                                         'webfonts!' => '',
                                         'iconfont-remote!' => '',
                                         'iconfont-name' => FONT_AWESOME,
                                         'linkcss' => '',
                                         'icons' => 'font')

      if @configuration.version
        ::Aged.logger.debug("revnumber='#{@configuration.version[:version_name]}'")
        git_rev = git_revision_string
        # asciidoctor.attributes_html << "revnumber='#{rev.strip}'" unless rev.empty?
        # asciidoctor.attributes_pdf << "revnumber='#{rev.strip}'" unless rev.empty?
        if @configuration.version[:branch] == '{git}'
          asciidoctor.attributes_html['revnumber'] = git_rev
          asciidoctor.attributes_pdf << "revnumber=#{git_rev}"
        else
          asciidoctor.attributes_html['revnumber'] = @configuration.version[:version_name]
          asciidoctor.attributes_pdf << "revnumber=#{@configuration.version[:version_name]}"
        end
      end

      asciidoctor.attributes_pdf << [
        "aged-version=#{AGED_VERSION}",
        'imagesdir=../images',
        'icons=font'
      ]

      apply_attributes(asciidoctor)
      asciidoctor.attributes_html.merge!(@configuration.attributes)

      @configuration.asciidoc_to_pdf.each do |adoc_pdf_file|
        if File.basename(adoc_file, '.*') == File.basename(adoc_pdf_file, '.*')
          asciidoctor.build_pdf = true
        end
      end

      asciidoctor.build
    end

    def build_sigle_page
      ::Aged.logger.debug('Build sigle page')
      asciidoctor = Asciidoctor.new("#{@configuration.single_page_template}.adoc", EXECUTER, build_dir: @configuration.build_dir)

      asciidoctor.build_html = @configuration.single_page_html
      asciidoctor.build_pdf = @configuration.single_page_pdf

      asciidoctor.requires_html << @configuration.requires
      asciidoctor.requires_pdf << @configuration.requires

      asciidoctor.attributes_html.merge!('aged-version' => AGED_VERSION,
                                         'rel_topdir' => @rel_topdir,
                                         'imagesdir' => 'images',
                                         'stylesdir' => "#{@rel_topdir}stylesheets",
                                         'webfonts!' => '',
                                         'iconfont-remote!' => '',
                                         'iconfont-name' => FONT_AWESOME,
                                         'includedir' => '../',
                                         'icons' => 'font')

      asciidoctor.attributes_html['data-uri'] if @configuration.single_page_embed_images
      asciidoctor.attributes_html['stylesheet'] = File.basename(@configuration.css_single) unless @configuration.css_single.empty?

      asciidoctor.attributes_pdf << [
        "aged-version=#{AGED_VERSION}",
        'imagesdir=../images',
        'includedir=../',
        'icons=font'
      ]

      if @configuration.version
        # rev = git_revision_string()
        ::Aged.logger.debug("revnumber='#{@configuration.version[:version_name]}'")
        asciidoctor.attributes_html['revnumber'] = @configuration.version[:version_name]
        asciidoctor.attributes_pdf << "revnumber=#{@configuration.version[:version_name]}"
      end

      apply_attributes(asciidoctor)
      asciidoctor.attributes_html.merge!(@configuration.attributes)

      # path = File.dirname(@configuration.single_page_template)

      @configuration.languages.each do |l|
        adoc_file = File.join(@configuration.docs_dir, "#{File.basename(@configuration.single_page_template, '.*')}_#{l}.adoc")
        ::Aged.logger.info("Building #{adoc_file}")

        FileUtils.cp_r "#{@configuration.single_page_template}.adoc", adoc_file

        asciidoctor_lang = asciidoctor.dup
        asciidoctor_lang.adoc_file = adoc_file
        asciidoctor_lang.attributes_html['lang'] = l
        asciidoctor_lang.attributes_pdf << "lang=#{l}"
        asciidoctor_lang.build(include_nav: false)
      end
    end

    private

    def git_revision_string
      `cd #{@configuration.project_dir}; git rev-parse --abbrev-ref HEAD`
    end

    def apply_attributes(asciidoctor)
      @configuration.attributes.each do |key, val|
        val = "#{@rel_topdir}#{val[2..-1]}" if val.nil? == false && val[0, 2] == './'
        asciidoctor.attributes_pdf << if val.to_s.empty?
                                        key
                                      else
                                        case key
                                        when 'source-highlighter'
                                          "#{key}=coderay"
                                        else
                                          "#{key}=#{val}"
                                        end
                                      end
      end
    end

    def set_up_output_dirs
      project_dir = @configuration.project_dir()
      build_dir = @configuration.build_dir()
      FileUtils.rm_rf build_dir
      FileUtils.mkdir_p build_dir

      ::Aged.logger.debug("Copying #{File.join('/', 'aged', 'images', '.')}")
      FileUtils.cp_r File.join('/', 'aged', 'images', '.'), File.join(build_dir, 'images')
      FileUtils.mkdir_p File.join(project_dir, 'docinfo')
      ::Aged.logger.debug("Copying #{File.join('/', 'aged', 'docinfo', 'docinfo.html')}")
      FileUtils.cp_r File.join('/', 'aged', 'docinfo', 'docinfo.html'), File.join(project_dir, 'docinfo', 'docinfo.html')
      ::Aged.logger.debug("Copying #{File.join('/', 'aged', 'docinfo', 'docinfo-footer.html')}")
      docinfo_footer_file = File.join(project_dir, 'docinfo', 'docinfo-footer.html')
      FileUtils.cp_r File.join('/', 'aged', 'docinfo', 'docinfo-footer.html'), docinfo_footer_file unless File.exist?(docinfo_footer_file)
      ::Aged.logger.debug('Copying stylesheets/.')
      FileUtils.cp_r 'stylesheets/.', File.join(@configuration.docs_dir, '/')
      ::Aged.logger.debug("Copying #{File.exist?(File.join(project_dir, @configuration.css))} #{File.join(project_dir, @configuration.css)}")
      css_file = File.join(project_dir, @configuration.css)
      FileUtils.cp_r css_file, File.join(@configuration.docs_dir, '/') if File.exist?(css_file)
      have_singe_css_file = !@configuration.css_single.empty? && File.exist?(File.join(project_dir, @configuration.css_single))
      ::Aged.logger.debug("Copying #{have_singe_css_file} #{File.join(project_dir, @configuration.css_single)}")
      FileUtils.cp_r File.join(project_dir, @configuration.css_single), File.join(@configuration.docs_dir, '/') if have_singe_css_file

      if @configuration.image_max_size?
        Dir.glob("#{project_dir}/images/**/*.*") do |image_file|
          next unless image_file.end_with?('.jpg', '.JPG', '.jpeg', '.JPEG', '.png', '.PNG')
          absolute_path = Pathname.new(File.expand_path(image_file))
          cmd = %(identify -format "%w" #{absolute_path})
          next unless `#{cmd}`.to_i > @configuration.image_max_size
          ::Aged.logger.info("Image size max #{@configuration.image_max_size}px width 300dpi #{absolute_path}")
          cmd = %(convert -density 300 -resize #{@configuration.image_max_size}\\> #{absolute_path} #{absolute_path})
          ::Aged.logger.debug(cmd)
          `#{cmd}`
        end
      end
      FileUtils.cp_r '/input/.aged.yml', File.join(project_dir, '.aged.yml')

      unless @rel_topdir == ''
        fix_docinfo(File.join(project_dir, 'docinfo', 'docinfo.html'), @rel_topdir)
        fix_docinfo(File.join(project_dir, 'docinfo', 'docinfo-footer.html'), @rel_topdir)
      end
      append_docinfo_footer(File.join(project_dir, 'docinfo', 'docinfo-footer.html'))
    end

    def build_to_output
      dest_path = if @configuration.version.nil? || @configuration.version[:master]
                    '/out'
                  else
                    File.join('/out', @configuration.version[:branch])
                  end

      Dir.chdir(@configuration.build_dir) do
        Dir.glob('*.*').select { |f| File.file?(f) }.each do |f|
          dest = File.join(dest_path, f)
          FileUtils.mkdir_p(File.dirname(dest))
          FileUtils.cp(f, dest)
        end
        Dir.glob('js/index_*.*').select { |f| File.file?(f) }.each do |f|
          dest = File.join(dest_path, f)
          FileUtils.mkdir_p(File.dirname(dest))
          FileUtils.cp(f, dest)
        end
      end

      copy_css

      FileUtils.cp_r File.join(@configuration.docs_dir, 'js', '.'), '/out/js' if File.exist?(File.join(@configuration.docs_dir, 'js'))

      copy_images(dest_path)
    end

    def copy_images(dest_path)
      copy_generated_images(dest_path)
      copy_project_images(dest_path)
    end

    def copy_generated_images(dest_path)
      return unless File.exist?(File.join(@configuration.docs_dir, 'images'))
      FileUtils.cp_r File.join(@configuration.docs_dir, 'images', '.'), File.join(dest_path, 'images')
    end

    def copy_project_images(dest_path)
      return unless File.exist?(File.join(@configuration.project_dir, 'images'))
      FileUtils.cp_r File.join(@configuration.project_dir, 'images', '.'), File.join(dest_path, 'images')
    end

    def copy_css
      FileUtils.mkdir_p('/out/stylesheets/') unless File.exist?('/out/stylesheets/')
      copy_asciidoc_generated_stylesheets
      copy_custom_css
      copy_cunstom_css_singlepage
    end

    def copy_asciidoc_generated_stylesheets
      return unless File.exist?(File.join(@configuration.docs_dir, 'stylesheets'))
      FileUtils.cp_r(File.join(@configuration.docs_dir, 'stylesheets', '.'), '/out/stylesheets/')
    end

    def copy_custom_css
      custom_css_path = File.join(@configuration.project_dir, @configuration.css)
      return unless @configuration.css != 'aged.min.css' && File.exist?(custom_css_path)
      ::Aged.logger.info("copy css stylesheets #{custom_css_path} to /out/stylesheets/")
      FileUtils.cp(custom_css_path, '/out/stylesheets/')
    end

    def copy_cunstom_css_singlepage
      return unless @configuration.css_single != '' && File.exist?(File.join(@configuration.project_dir, @configuration.css_single))
      ::Aged.logger.debug("copy single stylesheets #{File.join(@configuration.project_dir, @configuration.css_single)} to /out/stylesheets/")
      FileUtils.cp(File.join(@configuration.project_dir, @configuration.css_single), '/out/stylesheets/')
    end

    def fix_docinfo(docinfo_file, relative_path)
      text = File.read(docinfo_file)
      new_contents = text.gsub(%r{(href|src)\s*=\s*('|")(?!www\.|(http|ftp)s?:\/|[A-Za-z]:\\|\/\/)([^\/].*)('|")}, "\\1='#{relative_path}\\4'")

      # To write changes to the file, use:
      File.open(docinfo_file, 'w') { |file| file.puts new_contents }
    end

    def setup_build
      set_up_output_dirs

      setup_font_awesome
      setup_jquery
      setup_js_mark
      setup_js_polyfill
      setup_js_elasticlunr

      build_index if @configuration.navs_dir
    end

    def create_nav
      nav = {}
      @configuration.languages.each do |lang|
        nav[lang] = Asciidoctor.build_navigation(@configuration.navs_dir, lang, @configuration.attributes) if @configuration.navs_dir
      end
      nav
    end

    def build_index_for_languages
      @configuration.languages.each do |lang|
        cmd = %(build-index #{@configuration.build_dir} -l #{lang} -o #{File.join(@configuration.build_dir, 'js', "index_#{lang}.js")})
        ::Aged.logger.debug("Running #{cmd}")
        ::Aged.logger.info(`#{cmd}`)
      end
    end
  end
end
