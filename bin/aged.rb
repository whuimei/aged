require 'optparse'

require 'aged'
require 'aged/configuration'
require 'fileutils'
require 'shellwords'

#####
# $ apk add build-base ruby-dev
# $ gem install stackprof
#####
# require 'stackprof'

module Aged
  class <<self
    attr_accessor :logger
  end
end

::Aged.logger ||= Logger.new(STDOUT)
::Aged.logger.level = Logger::INFO
::Aged.logger.formatter = proc do |severity, datetime, _progname, msg|
  date_format = datetime.strftime('%Y-%m-%d %H:%M:%S')
  "[#{date_format}] AGED: #{severity}: #{msg}\n"
end

def append_docinfo_footer(docinfo_file)
  ::Aged.logger.info('Compiling aged.js')

  javascript = File.read('lib/aged.js')

  ret = %x(echo #{Shellwords.escape(javascript)} | closure-compiler)
  t = %{<button onclick="$(window).scrollTop(0)" id="to-top-btn" class="hidden" title="Go to top"><i class="fa fa-chevron-up"></i></button>
    <script>#{ret}</script>
  }

  File.write(docinfo_file, t, File.size(docinfo_file), mode: 'a')
end

def find_root(docs_dir, includes, file)
  root = []
  file_removed_docs_dir = file.dup
  file_removed_docs_dir.slice!(docs_dir + '/')
  if File.dirname(file_removed_docs_dir) != '.'
    ::Aged.logger.debug("Found parents #{includes[file]}")
    includes[file].each do |parent|
      root |= find_root(docs_dir, includes, parent)
    end
  else
    ::Aged.logger.debug("Found root #{file}")
    root = [file]
  end
  root
end

def find_root_files(configuration, file)
  if File.dirname(file) != '.'
    includes = {}
    Dir.glob(File.join(configuration.docs_dir, '**/*.adoc')).each do |f|
      dir = File.dirname(f)
      file_content = File.readlines(f)

      variables = file_content.each_with_object({}) do |a, n|
        if n =~ /^:(\w+):\s+(.*)/
          a["{#{Regexp.last_match(1)}}"] ||= Regexp.last_match(2)
        else
          a
        end
      end

      replace_var = lambda do |var|
        variables.each { |k, v| var.sub!(k, v) }
        var
      end

      file_content.each_with_object(includes) do |a, n|
        (a[File.expand_path(File.join(dir, replace_var.call(Regexp.last_match(1))))] ||= []) << f if n =~ /^include::([^\[]+)\[[^\]]*\]\s*$/
        a
      end
    end
    file = File.join(configuration.docs_dir, file)
    roots = find_root(configuration.docs_dir, includes, file)
  else
    roots = [File.join(configuration.docs_dir, file)]
  end
  ::Aged.logger.info("Files to build #{roots}")
  roots
end

def version(version, master)
  return nil unless version

  l = version.split(';')
  bransh = l[0]
  version_name = if l.length == 2
                   l[1]
                 else
                   l[0]
                 end
  {
    branch: bransh.strip,
    version_name: version_name.strip,
    master: master
  }
end

configuration = Aged::Configuration.new
aged = Aged::Aged.new()

project_dir = '/aged/project/'

FileUtils.rm_rf project_dir
FileUtils.mkdir_p project_dir

build_dir = File.join(project_dir, 'build')

configuration.load_yaml('/input/.aged.yml')
configuration.project_dir = project_dir
configuration.build_dir = build_dir

opts = OptionParser.new do |opt|
  opt.banner = 'Usage: aged [options] [file]'

  opt.on('-v', '--[no-]verbose', 'Run verbosely') do |v|
    ::Aged.logger.level = Logger::DEBUG if v
  end

  opt.on('-a NAME[=VALUE]', '--attribute NAME[=VALUE]', 'Asciidoctor attributes') do |a|
    key, val = a.split('=')
    configuration.set_attribute(key, val)
  end

  opt.on('-r NAME', '--require NAME', 'Asciidoctor requires') do |r|
    configuration.add_require(r)
  end

  opt.on('-h', '--help', 'Prints this help') do
    puts opt
    exit
  end
end.parse!

configuration.version = version(configuration.versions.first, true) unless configuration.versions.nil?

::Aged.logger.info('Buildin version main')

::Aged.logger.debug("Copy project to temp working directory #{project_dir}")
FileUtils.cp_r '/input/.', project_dir
FileUtils.cp_r(File.join(project_dir, configuration.css), configuration.docs_dir) unless configuration.css == 'aged.min.css'
FileUtils.cp_r(File.join(project_dir, configuration.single_page_css), configuration.docs_dir) unless configuration.single_page_css.nil?

FileUtils.cp_r '/output/images/.', File.join(project_dir, 'images') if File.exist?('/output/images')
FileUtils.cp_r '/output/images/.', File.join(project_dir, 'docs', 'images') if File.exist?('/output/images')

# ::Aged.logger.info("start stackprof")
# StackProf.run(mode: :cpu, out: 'tmp/stackprof-cpu-myapp.dump', raw: true) do
if !opts.empty?
  opts.each do |file|
    files = find_root_files(configuration, file)
    files.each do |f|
      aged.build_file(configuration, f)
    end
  end
else
  aged.build(configuration)
end
# end

FileUtils.cp_r File.join(build_dir, 'stylesheets', '.'), '/out/stylesheets/'
FileUtils.cp_r File.join(build_dir, 'fonts', '.'), '/out/fonts/'
FileUtils.cp_r File.join(build_dir, 'js', '.'), '/out/js/'

FileUtils.cp_r "/aged/project/#{configuration.translations_dir}/.", "/input/#{configuration.translations_dir}" if configuration.have_translations

configuration.copy.each do |copy|
  FileUtils.cp_r File.join(project_dir, copy['from']), File.join('/', 'out', copy['to'])
end

unless configuration.versions().nil?
  ::Aged.logger.info("Multiple versions #{configuration.versions}")
  configuration.versions.drop(1).each do |v|
    ::Aged.logger.info("Buildin version #{v}")
    configuration.version = version(v, false)

    project_dir = "/aged/project/#{configuration.version[:branch]}"
    build_dir = File.join(project_dir, 'build')
    configuration.project_dir = project_dir
    configuration.build_dir = build_dir

    FileUtils.cp_r '/input/.', project_dir

    next unless system("cd #{project_dir}; git checkout .; git checkout #{configuration.version[:branch]}")

    FileUtils.cp_r File.join('/', 'output', configuration.version[:branch], 'images', '.'), File.join(project_dir, 'images') if File.exist?(File.join('/', 'output', configuration.version[:branch], 'images'))
    FileUtils.cp_r File.join('/', 'output', configuration.version[:branch], 'images', '.'), File.join(project_dir, 'docs', 'images') if File.exist?(File.join('/', 'output', configuration.version[:branch], 'images'))

    ret = `cd #{project_dir}; git clean -xdf`
    puts ret unless ret.empty?

    configuration_v = Aged::Configuration.new
    configuration_v.load_yaml(File.join(project_dir, '.aged.yml'))
    configuration.start_page = configuration_v.start_page

    aged.build(configuration)
  end
end

FileUtils.cp_r File.join('/', 'aged', 'stylesheets', 'aged.min.css'), File.join('/', 'out', 'stylesheets')
FileUtils.cp_r File.join('/', 'aged', 'stylesheets', 'aged.css'), File.join('/', 'out', 'stylesheets')
FileUtils.cp_r File.join('/', 'aged', 'stylesheets', 'coderay-asciidoctor.css'), File.join('/', 'out', 'stylesheets') if configuration.get_attribute('source-highlighter') == 'coderay'
FileUtils.cp_r '/out/.', '/output'
