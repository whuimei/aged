// Swedish translation
:appendix-caption: Bilaga
:appendix-refsig: {appendix-caption}
:caution-caption: Observera
:chapter-label: Kapitel
:chapter-refsig: {chapter-label}
:example-caption: Exempel
:figure-caption: Figur
:important-caption: Viktigt
:last-update-label: Senast uppdaterad
ifdef::listing-caption[:listing-caption: Listing]
//:manname-title: Namn
:note-caption: Anm
:part-refsig: Del
ifdef::preface-title[:preface-title: Preface]
:section-refsig: Avsnitt
:table-caption: Tabell
:tip-caption: Tips
:toc-title: Innehållsförteckning
:untitled-label: Untitled
:version-label: Version
:warning-caption: Varning
