require "aged/configuration"

describe Aged do
  describe "is given an config" do
    context "that is empty" do
      it 'raises an error' do
        aged_config = Aged::Configuration.new
        expect {aged_config.load_yaml('spec/aged/data/empty.yml')}.to raise_error(RuntimeError, 'Config empty')
        expect {aged_config.config = nil}.to raise_error(RuntimeError, 'Config empty')
      end
    end

    context "missing project" do
      it 'rases an error' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        cnf.delete("project")

        aged_config = Aged::Configuration.new
        expect {aged_config.config = cnf}.to raise_error(RuntimeError, 'Config missing project')
      end
    end

    context "missing docs" do
      it 'rases an error' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        cnf.delete("docs")

        aged_config = Aged::Configuration.new
        aged_config.config = cnf
        expect(aged_config.docs_dir()).to eql("./docs")
      end
    end

    context "missing start_page" do
      it 'rases an error' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        cnf.delete("start_page")

        aged_config = Aged::Configuration.new
        expect {aged_config.config = cnf}.to raise_error(RuntimeError, 'Config missing start_page')
      end
    end

    context "start_page" do
      it '.start_page()' do
        aged_config = Aged::Configuration.new
        cnf = YAML::load_file('spec/aged/data/config.yml')
        cnf["start_page"] = "start_here"
        aged_config.config = cnf


        expect(aged_config.start_page()).to eql("start_here")
      end
    end

    context "invalid languages" do
      it 'rases an error' do
        aged_config = Aged::Configuration.new

        cnf = YAML::load_file('spec/aged/data/config.yml')

        cnf.delete("languages")
        expect {aged_config.config = cnf}.to raise_error(RuntimeError, 'Config missing languages')

        cnf["languages"] = "not valid"
        expect {aged_config.config = cnf}.to raise_error(RuntimeError, 'Config languages must be an array with atleast one language')

        cnf["languages"] = []
        expect {aged_config.config = cnf}.to raise_error(RuntimeError, 'Config languages must be an array with atleast one language')
      end
    end

    context "languages" do
      it '.languages()' do
        aged_config = Aged::Configuration.new
        cnf = YAML::load_file('spec/aged/data/config.yml')

        cnf["languages"] = ["en_GB"]
        aged_config.config = cnf
        expect(aged_config.languages()).to eql(["en"])

        cnf["languages"] = ["en_GB","sv_SE"]
        aged_config.config = cnf
        expect(aged_config.languages()).to eql(["en","sv"])
      end
    end

    context "countries" do
      it '.countries()' do
        aged_config = Aged::Configuration.new
        cnf = YAML::load_file('spec/aged/data/config.yml')

        cnf["languages"] = ["en_GB"]
        aged_config.config = cnf
        expect(aged_config.countries()).to eql(["GB"])

        cnf["languages"] = ["en_GB","sv_SE"]
        aged_config.config = cnf
        expect(aged_config.countries()).to eql(["GB","SE"])
      end
    end

    context "docs dir" do
      it '.docs_dir()' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        aged_config = Aged::Configuration.new

        cnf['docs'] = "docs"
        aged_config.config = cnf
        expect(aged_config.docs_dir()).to eql("./docs")

        aged_config.project_dir = "/base/path"
        expect(aged_config.docs_dir()).to eql("/base/path/docs")
      end
    end

    context "nav dir" do
      it '.navs_dir()' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        aged_config = Aged::Configuration.new

        cnf['nav_dir'] = "nav"
        aged_config.config = cnf
        expect(aged_config.navs_dir()).to eql("./nav")

        aged_config.project_dir = "/base/path"
        expect(aged_config.navs_dir()).to eql("/base/path/nav")

        cnf.delete("nav_dir")
        aged_config.config = cnf
        expect(aged_config.navs_dir()).to eql("/base/path/")
      end
    end

    context "that has not set css" do
      it '.css() returns the default' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        aged_config = Aged::Configuration.new

        cnf.delete("css")
        aged_config.config = cnf
        expect(aged_config.css()).to eql("aged.min.css")
      end
    end

    context "translations" do
      it 'supplied translations' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        aged_config = Aged::Configuration.new

        cnf.delete("pot")
        aged_config.config = cnf
        expect(aged_config.have_translations()).to eql(false)

        cnf['pot'] = {}
        aged_config.config = cnf

        allow(Dir).to receive(:glob).with("#{aged_config.docs_dir()}/**/*.adoc").and_return(["file1.adoc", "file2.adoc"])
        allow(Dir).to receive(:glob).with("#{aged_config.project_dir()}/file3.adoc").and_return(["file3.adoc"])
        allow(Dir).to receive(:glob).with("#{aged_config.project_dir()}/file4.adoc").and_return(["file4.adoc"])
        allow(Dir).to receive(:glob).with("./test/**/*.adoc").and_return(["file5.adoc", "file6.adoc"])

        expect(aged_config.have_translations()).to eql(true)
        expect(aged_config.translations_dir()).to eql("po")
        expect(aged_config.translations_min_level()).to eql(80)
        expect(aged_config.translations_files()).to eql(["file1.adoc", "file2.adoc"])

        cnf['pot']['files'] = ["file3.adoc", "file4.adoc"]
        aged_config.config = cnf
        expect(aged_config.translations_files()).to eql(["file3.adoc", "file4.adoc" ])

        cnf['pot']['files'] = ["test/**/*.adoc","file3.adoc", "file4.adoc"]
        aged_config.config = cnf
        expect(aged_config.translations_files()).to eql(["file5.adoc", "file6.adoc", "file3.adoc", "file4.adoc" ])
      end
    end

    context "requires" do
      it 'requires()' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        aged_config = Aged::Configuration.new

        cnf.delete("requires")
        aged_config.config = cnf
        expect(aged_config.requires()).to eql([])

        cnf['requires'] = ["asciidoctor-diagram"]
        aged_config.config = cnf
        expect(aged_config.requires()).to eql(["asciidoctor-diagram"])
      end
    end

    context "attributes" do
      it 'attributes()' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        aged_config = Aged::Configuration.new

        cnf.delete("attributes")
        aged_config.config = cnf
        expect(aged_config.attributes()).to eql({})

        cnf['attributes'] = {:icons => "font"}
        aged_config.config = cnf
        expect(aged_config.attributes()[:icons]).to eql("font")
      end
    end

    context "single_page" do
      it 'single_page_template()' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        aged_config = Aged::Configuration.new

        cnf.delete("single_page")
        aged_config.config = cnf

        expect(aged_config.single_page_template().nil?).to eql(true)

        cnf['single_page'] = {}
        aged_config.config = cnf
        expect(aged_config.single_page_template().nil?).to eql(true)

        cnf['single_page'] = {'template' => "aged_single_page"}
        aged_config.config = cnf
        expect(aged_config.single_page_template()).to eql("./aged_single_page")

        aged_config.project_dir = "/base/path"
        expect(aged_config.single_page_template()).to eql("/base/path/aged_single_page")
      end

      it 'single_page_embed_images()' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        aged_config = Aged::Configuration.new

        cnf.delete("single_page")
        aged_config.config = cnf
        expect(aged_config.single_page_embed_images()).to eql(false)

        cnf['single_page'] = {'embed_images' => true }
        aged_config.config = cnf
        expect(aged_config.single_page_embed_images()).to eql(true)

        cnf['single_page'] = {'embed_images' => false }
        aged_config.config = cnf
        expect(aged_config.single_page_embed_images()).to eql(false)
      end

      it 'single_page_html()' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        aged_config = Aged::Configuration.new

        cnf.delete("single_page")
        aged_config.config = cnf
        expect(aged_config.single_page_html()).to eql(false)

        cnf['single_page'] = {'format' => ["html"]}
        aged_config.config = cnf
        expect(aged_config.single_page_html()).to eql(true)

        cnf['single_page'] = {'format' => ["pdf", "html"]}
        aged_config.config = cnf
        expect(aged_config.single_page_html()).to eql(true)

        cnf['single_page'] = {'format' => ["pdf"]}
        aged_config.config = cnf
        expect(aged_config.single_page_html()).to eql(false)

        cnf['single_page'] = {'format' => []}
        aged_config.config = cnf
        expect(aged_config.single_page_html()).to eql(false)
      end

      it 'single_page_pdf()' do
        cnf = YAML::load_file('spec/aged/data/config.yml')
        aged_config = Aged::Configuration.new

        cnf.delete("single_page")
        aged_config.config = cnf

        expect(aged_config.single_page_pdf()).to eql(false)

        cnf['single_page'] = {'format' => ["html"]}
        aged_config.config = cnf
        expect(aged_config.single_page_pdf()).to eql(false)

        cnf['single_page'] = {'format' => ["pdf", "html"]}
        aged_config.config = cnf
        expect(aged_config.single_page_pdf()).to eql(true)

        cnf['single_page'] = {'format' => ["pdf"]}
        aged_config.config = cnf
        expect(aged_config.single_page_pdf()).to eql(true)

        cnf['single_page'] = {'format' => []}
        aged_config.config = cnf
        expect(aged_config.single_page_pdf()).to eql(false)
      end
    end

#      template: aged_single_page
#      format:
#        - html
#        - pdf
  end
end
