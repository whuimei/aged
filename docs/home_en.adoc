//po4a: entry lang
:lang: en
:doctype: first_page
:chapter-label:
include::../common_parameters.adoc[]

= AGED

include::_parts/_short_introduction_{lang}.adoc[leveloffset=+1]

'''

== Overview

[role="grouping"]
--
[role="card"]
.Documentation
****
icon:book[float="left",role="initial"]
For information about how to use AGED.

'''
xref:getting_started_{lang}.adoc#[]:: [.small]#To get started quickly#
xref:user_manual_{lang}.adoc#[]:: [.small]#In depth documentation#
xref:extensions_{lang}.adoc#[]:: [.small]#Available extentions#
****

[role="card"]
.Development
****
icon:wrench[float="left",role="initial"]
For information about the AGED project it self and how to contrubute.

'''
xref:development_{lang}.adoc#_structure[]:: [.small]#How to contribute#
****

[role="card"]
.Translation
****
icon:language[float="left",role="initial"]
Information about how to translate and how translations is handled in AGED.

'''
xref:user_manual_{lang}.adoc#_adding_translations[]:: [.small]#How to use#
xref:development_{lang}.adoc#_translation[]:: [.small]#How translations is done#
****
--
