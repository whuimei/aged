#!/bin/bash
poll=false
while getopts 'ph' flag; do
  case "${flag}" in
    h)
      echo "watcher.sh: detects document file changes"
      echo " "
      echo "watcher.sh [options]"
      echo " "
      echo "options:"
      echo "-h, --help                show brief help"
      echo "-p, --poll       use poll insted of events"
      exit 0
      ;;
    p)
      poll=true;;
    *)
      break;;
  esac
done

echo "Build docs dir"
ruby -I /aged/lib/ bin/aged.rb

watch="/input/docs/"

if [ "$poll" = true ]; then
  echo "Polling docs dir"

  declare -A file_array

  while IFS=  read -r -d $'\0'; do
    file_array[$REPLY]=$(md5sum $REPLY | awk '{ print $1 }')
  done < <(find $watch -type f -iname "*.adoc" -print0)

  while : ; do
    sleep 5
    while IFS=  read -r -d $'\0'; do
      f=$(md5sum $REPLY | awk '{ print $1 }')
      if [[ "${file_array[$REPLY]}" != "$f" ]]; then
        echo "$REPLY old ${file_array[$REPLY]} new $f"
        ruby -I /aged/lib/ bin/aged.rb $filename &
        file_array[$REPLY]=$f
      fi
    done < <(find $watch -type f -iname "*.adoc" -print0)
  done
else
  echo "Watching docs dir"
  inotifywait -e close_write -m $watch |
  while read -r directory events filename; do
    ruby -I /aged/lib/ bin/aged.rb $filename &
  done
fi
