FROM node:alpine AS compass

RUN apk update && apk add ca-certificates && update-ca-certificates && apk add openssl wget build-base libffi-dev ruby ruby-dev
RUN gem install --version '4.3.2' zurb-foundation
RUN gem install sass compass
RUN npm install clean-css-cli -g
RUN apk del build-base libffi-dev ruby-dev

WORKDIR /opt

RUN wget http://github.com/asciidoctor/asciidoctor-stylesheet-factory/archive/master.zip
RUN unzip master.zip

COPY sass /opt/asciidoctor-stylesheet-factory-master/sass
COPY docker/build_css.sh /opt/asciidoctor-stylesheet-factory-master/build_css.sh

WORKDIR /opt/asciidoctor-stylesheet-factory-master

FROM compass AS build_compass
RUN ./build_css.sh aged
# RUN compass compile -s compressed


FROM alpine:edge AS build_po4a
ADD https://github.com/freke/po4a/archive/master.zip /tmp/po4a.zip
RUN unzip /tmp/po4a.zip


FROM freke/docker_asciidoctor
RUN apk update; \
    apk upgrade; \
    apk add --no-cache git imagemagick inotify-tools perl gettext perl-unicode-linebreak diffutils nodejs npm closure-compiler@testing; \
    rm -rf /var/cache/apk/*; \
    mkdir -p /aged/project; \
    chmod u+rw,g+rw,o+rw /aged/project; \
    mkdir /out; \
    chmod u+rw,g+rw,o+rw /out

COPY --from=build_po4a /po4a-master/lib /po4a/lib
COPY --from=build_po4a /po4a-master/po /po4a/po
COPY --from=build_po4a /po4a-master/extension /po4a/extension
COPY --from=build_po4a /po4a-master/scripts /po4a/scripts
COPY --from=build_po4a /po4a-master/share /po4a/share
COPY --from=build_po4a /po4a-master/po4a* /po4a/
COPY --from=build_compass /opt/asciidoctor-stylesheet-factory-master/aged.min.css /aged/stylesheets/
COPY --from=build_compass /opt/asciidoctor-stylesheet-factory-master/aged.css /aged/stylesheets/
ADD https://raw.githubusercontent.com/asciidoctor/asciidoctor/master/data/stylesheets/coderay-asciidoctor.css /aged/stylesheets/

ENV PERLLIB /po4a/lib
ENV PATH="/po4a:${PATH}"

ADD https://code.jquery.com/jquery-3.3.1.min.js /aged/js/jquery-3.3.1.min.js
ADD http://elasticlunr.com/elasticlunr.min.js /aged/js/elasticlunr.min.js
ADD https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.1/jquery.mark.min.js /aged/js/jquery.mark.min.js
ADD https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.6.0/polyfill.min.js /aged/js/polyfill.min.js

ADD https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css /aged/css/font-awesome.min.css
ADD https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2 /aged/fonts/fontawesome-webfont.woff2
ADD https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.woff /aged/fonts/fontawesome-webfont.woff
ADD https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.svg /aged/fonts/fontawesome-webfont.svg
ADD https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/fonts/fontawesome-webfont.ttf /aged/fonts/fontawesome-webfont.ttf

COPY docinfo/ /aged/docinfo/
COPY extensions/ /aged/extensions/
COPY images/gitbranch.svg /aged/images/
COPY images/favicon /aged/images/favicon
COPY lib/ /aged/lib/
COPY package.json /aged/package.json
COPY bin/aged.rb /aged/bin/aged.rb
COPY docker/watcher.sh /watcher.sh

RUN chmod +x /aged/lib/build-index.js; \
    chmod +r /aged/js/jquery-3.3.1.min.js; \
    chmod +r /aged/js/jquery.mark.min.js; \
    chmod +r /aged/js/polyfill.min.js; \
    chmod +r /aged/js/elasticlunr.min.js; \
    chmod +r /aged/css/font-awesome.min.css; \
    chmod +r /aged/fonts/fontawesome-webfont.woff2; \
    chmod +r /aged/fonts/fontawesome-webfont.woff; \
    chmod +r /aged/fonts/fontawesome-webfont.svg; \
    chmod +r /aged/fonts/fontawesome-webfont.ttf; \
    chmod -R +r /aged/stylesheets/

RUN closure-compiler --js=/aged/lib/elasticlunr_lang/lunr.multi.js > /aged/lib/elasticlunr_lang/lunr.multi.min.js; \
    closure-compiler --js=/aged/lib/elasticlunr_lang/lunr.stemmer.support.js > /aged/lib/elasticlunr_lang/lunr.stemmer.support.min.js; \
    closure-compiler --js=/aged/lib/elasticlunr_lang/lunr.ja.js > /aged/lib/elasticlunr_lang/lunr.ja.min.js; \
    closure-compiler --js=/aged/lib/elasticlunr_lang/lunr.sv.js > /aged/lib/elasticlunr_lang/lunr.sv.min.js; \
    closure-compiler --js=/aged/lib/elasticlunr_lang/lunr.zh.js > /aged/lib/elasticlunr_lang/lunr.zh.min.js

VOLUME /input
VOLUME /output

WORKDIR /aged

RUN npm link

ENTRYPOINT ["ruby", "-I", "/aged/lib/", "bin/aged.rb"]
CMD []
