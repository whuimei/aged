FROM alpine:edge

RUN echo "@testing http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories

RUN apk update && apk upgrade && apk --update add \
    ruby ruby-irb ruby-rake ruby-io-console ruby-bigdecimal \
    libstdc++ tzdata

RUN gem install bundler --no-document \
    && rm -r /root/.gem \
    && find / -name '*.gem' | xargs rm

RUN apk --update add --virtual build_deps \
    build-base ruby-dev libc-dev linux-headers \
    openssl-dev libxml2-dev libxslt-dev libffi-dev git && \
    echo "gem: --no-document" >> /etc/gemrc && \
    bundle config build.nokogiri --use-system-libraries && \
    gem install rspec cucumber ffi && \
    gem install nokogiri -- --use-system-libraries && \
    apk del build_deps

RUN rm -rf /var/cache/apk/*

VOLUME /test

WORKDIR /test
